package ru.pae.baggage;

/**
 * @author Petrunin Aleksey 18IT18
 */

import java.util.ArrayList;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Сколько пассажиров вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Baggage> baggages = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            baggages.add(input());
        }
        pasClad(baggages);
    }

    private static Baggage input() {
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.println("Укажите количество багажных мест: ");
        int number = scanner.nextInt();
        System.out.print("Общий вес багажа : ");
        double weight = scanner.nextDouble();
        return new Baggage(surname, number, weight);
    }

    private static void pasClad(ArrayList<Baggage> baggages) {
        for (Baggage baggage : baggages) {
            if (baggage.Сlad()) {
                System.out.println(baggage.getSurname() + " с ручной кладью");
            }
        }
    }
}
